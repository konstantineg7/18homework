﻿
#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

#include <iomanip>


template <typename T>
class Stack
{
private:
    T* stackPtr; // указатель на стек
    int size; // размер стека
    T top; // вершина стека
public:
    Stack(int = 10);// по умолчанию размер стека равен 10 элементам
    ~Stack(); // деструктор
    bool push(const T); // поместить элемент в стек
    bool pop(); // удалить из стека элемент
    void printStack();
};

 
int main()
{
    setlocale(LC_ALL, "RUS");
    /*cout << "введите тип данных :" << endl << "int-1" << endl << "float-2"
        << endl << "double-3" << endl << "string-4"<<endl;
    int TypeD;
    cin >> TypeD;
  
    switch (TypeD)
    { 
    case 1:
        cout << "выбран тип данных int"<<endl;
        {
            Stack <int> myStack(5);
            cout << "Заталкиваем элементы в стек: ";
            int ct = 0;
            while (ct++ != 5)
            {
                int temp;
                cin >> temp;
                myStack.push(temp);
            }

            myStack.printStack();
        }

        break;
    case 2:
        cout << "выбран тип данных float" << endl;
        {
            Stack <float> myStack(5);
            cout << "Заталкиваем элементы в стек: ";
            int ct = 0;
            while (ct++ != 5)
            {
               float temp;
                cin >> temp;
                myStack.push(temp);
            }

            myStack.printStack();
        }

        break;
    case 3:
        cout << "выбран тип данных double" << endl;
        {
            Stack <double> myStack(5);
            cout << "Заталкиваем элементы в стек: ";
            int ct = 0;
            while (ct++ != 5)
            {
                double temp;
                cin >> temp;
                myStack.push(temp);
            }

            myStack.printStack();
        }
        break;
    case 4:
        cout << "выбран тип данных string" << endl;
        {
            Stack <string> myStack(5);
            cout << "Заталкиваем элементы в стек: ";
            int ct = 0;
            while (ct++ != 5)
            {
                string temp;
                cin >> temp;
                myStack.push(temp);
            }

            myStack.printStack();
        }
        break;
        return true;
    default:
        return false;
    }
  */
    
    
    
    Stack <int> myStack(5);
    cout << "добавляем элементы в стек: ";
    int ct = 0;
    while (ct++ != 5)
    {
        int temp;
        cin >> temp;
        myStack.push(temp);
    }

    myStack.printStack(); 
    
 
    myStack.pop(); 

    cout << endl;
    myStack.printStack(); 
    return 0; 
}
// конструктор
template <typename T>
Stack<T>::Stack(int s)
{
    size = s > 0 ? s : 10;   // инициализировать размер стека
    stackPtr = new T[size]; // выделить память под стек
    top = -1; // значение -1 говорит о том, что стек пуст
}

// деструктор
template <typename T>
Stack<T>::~Stack()
{
    delete[] stackPtr; // удаляем стек
}

// элемент функция класса  Stack для помещения элемента в стек
// возвращаемое значение - true, операция успешно завершена
//                                    false, элемент в стек не добавлен
template <typename T>
bool Stack<T>::push(const T value)
{
    if (top == size - 1)
        return false; // стек полон

    top++;
    stackPtr[top] = value; // помещаем элемент в стек

    return true; // успешное выполнение операции
}

// элемент функция класса  Stack для удаления элемента из стек
// возвращаемое значение - true, операция успешно завершена
//                                    false, стек пуст
template <typename T>
bool Stack<T>::pop()
{
    if (top == -1)
        return false; // стек пуст

    stackPtr[top] = 0; // удаляем элемент из стека
    top--;

    return true; // успешное выполнение операции
}

// вывод стека на экран
template <typename T>
void Stack<T>::printStack()
{
    for (int ix = size - 1; ix >= 0; ix--)
        cout << "|" << setw(4) << stackPtr[ix] << endl;
}